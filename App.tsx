import React from "react";

import { createAppContainer, createSwitchNavigator} from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Ionicons } from '@expo/vector-icons';

import Home from './src/views/Home';
import Profile from './src/views/Profile';

const AppContainer = createStackNavigator(
    {
        default: createBottomTabNavigator(
            {
                Home: {
                    screen: Home,
                    navigationOptions: {
                        tabBarIcon: ({tintColor}) => <Ionicons name="ios-home" size={24} color={tintColor}/>
                    }
                },
                Flashlight: {
                    screen: Home,
                    navigationOptions: {
                        tabBarIcon: ({}) => <Ionicons
                            name="ios-flashlight"
                            size={48}
                            color="#00ffcd"
                            style={{
                               shadowColor: "#00ffcd",
                               shadowOffset: { width: 0, height: 0 },
                               shadowRadius: 10,
                               shadowOpacity: 0.3
                           }}/>
                    }
                },
                Profile: {
                    screen: Profile,
                    navigationOptions: {
                        tabBarIcon: ({tintColor}) => <Ionicons name="ios-person" size={24} color={tintColor} />

                    }
                },
            },
            {
                defaultNavigationOptions: {
                    tabBarOnPress: ({navigation, defaultHandler}) => {
                        if (navigation.state.key === 'Flashlight') {
                            alert("Catcha, you have been flashed")
                        }
                        else {
                            defaultHandler();
                        }
                    }
                },
                tabBarOptions: {
                    activeTintColor: "#161F3D",
                    inactiveTintColor: "#B8BBC4",
                    showLabel: false
                }
            }
        )
    },
    {
        headerMode: "none",
    }
);

export default createAppContainer(
    createSwitchNavigator(
        {
            App: AppContainer,
        },
        {
            initialRouteName: "App"
        }
    )
);