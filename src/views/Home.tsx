import React from "react";
import {View, Text, StyleSheet, FlatList, Image, StatusBar, Platform, SafeAreaView} from "react-native";

import MapView from 'react-native-maps';

export default class Home extends React.Component<HomeProps, HomeState> {
    render() {
        return(
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle={"dark-content"}/>
                <MapView style={styles.mapStyle} />
            </SafeAreaView>
        )
    }
};

interface HomeProps extends React.ComponentProps<any> {

}

interface HomeState {

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#EFECF4",
    },
    header: {
        paddingTop: 64,
        paddingBottom: 16,
        backgroundColor: "#FFF",
        alignItems: "center",
        justifyContent: "center",
        borderBottomWidth: 1,
        borderBottomColor: "#EBECF4",
        shadowColor: "#454D65",
        shadowOffset: { height: 5, width: 0 },
        shadowRadius: 15,
        shadowOpacity: 0.2,
        zIndex: 10
    },
    headerTitle: {
        fontSize: 20,
        fontWeight: "500",
    },
    mapStyle: {
        width: '100%',
        height: '100%',
    },
})
